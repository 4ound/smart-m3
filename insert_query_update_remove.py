from smart_m3.m3_kp_api import *

AGENT_1_TRIPLE = Triple(URI('agent1'), None, None)
AGENT_3_WITH_MESSAGE_TRIPLE = Triple(URI('agent3'), URI('hello agent1!'), Literal(0))
AGENT_1_WITH_LITERAL_1_TRIPLE = Triple(URI('agent1'), None, Literal(1))

def main():
    kp = m3_kp_api()

    kp.clean_sib()

    insert_list = [
        Triple(URI('agent1'), URI('hello agent2!'), Literal(0)),
        Triple(URI('agent1'), URI('grate to see you!'), Literal(1)),
        Triple(URI('agent2'), URI('hello agent3!'), Literal(0)),
        Triple(URI('agent3'), URI('hello agent1!'), Literal(0)),
    ]

    kp.load_rdf_insert(insert_list)

    print(f"inserted {insert_list}")

    print_query(kp, AGENT_3_WITH_MESSAGE_TRIPLE)

    print_query(kp, AGENT_1_TRIPLE)

    kp.load_rdf_remove(AGENT_1_WITH_LITERAL_1_TRIPLE)

    print_query(kp, AGENT_1_TRIPLE)

    new_data = [Triple(e[0], e[1], Literal(int(e[2].value) + 10)) for e in kp.result_rdf_query]
    print(f"{new_data=}")

    kp.load_rdf_update(new_data, kp.result_rdf_query)

    print_query(kp, AGENT_1_TRIPLE)

    kp.clean_sib()

    kp.leave()


def print_query(kp, triple):
    kp.load_query_rdf(triple)
    print(f"query {triple}: {kp.result_rdf_query}")


if __name__ == '__main__':
    main()
