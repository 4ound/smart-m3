from smart_m3.m3_kp_api import *
from time import sleep
from datetime import datetime
import os

AGENT = URI("agent")
MESSAGE = URI("message")


def triple(i):
    return Triple(AGENT, MESSAGE, Literal(i))


def main():
    kp = m3_kp_api()
    kp.clean_sib()

    handler1 = KPHandler1(kp)
    handler2 = KPHandler2(kp)

    sub1 = kp.load_subscribe_RDF(Triple(AGENT, MESSAGE, None), handler1)
    sub2 = kp.load_subscribe_RDF(Triple(AGENT, MESSAGE, None), handler2)

    for i in range(6):
        sleep(1)
        kp.load_rdf_insert([triple(i)])

    kp.load_unsubscribe(sub1)
    kp.load_unsubscribe(sub2)

    print_query(kp, Triple(AGENT, MESSAGE, None))

    kp.clean_sib()
    kp.leave()

    raise os._exit(0)


def print_query(kp, t):
    kp.load_query_rdf(t)
    print(f"query {t}: {kp.result_rdf_query}")


class KPHandler1:
    def __init__(self, kp: m3_kp_api = None):
        self.kp = kp

    def handle(self, added, removed):
        # in case if you want to react on added/removed data - just use self.kp.your_function(....) here
        print(f'Agent_X {datetime.now()} reporting: {added=}, {removed=}')


class KPHandler2:
    def __init__(self, kp: m3_kp_api = None):
        self.kp = kp

    def handle(self, added, removed):
        for t in added:
            s, p, o = t
            if int(o.value) % 2 == 0:
                self.kp.load_rdf_remove(t)


if __name__ == '__main__':
    main()
