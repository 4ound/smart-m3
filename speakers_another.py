from smart_m3.m3_kp_api import *
from time import sleep
from datetime import datetime
import os

AGENT = URI("agent")
HUMAN = URI("human")
MESSAGE = URI("message")
SPEAKERS_NUM = 3


def main():
    kp = m3_kp_api()
    kp.clean_sib()

    text = input("Input your text here: ")

    stop_checker = StopChecker(kp)
    # интересно было проверить, можно ли подписать на несколько топиков
    stop_checker_sub_1 = kp.load_subscribe_RDF(Triple(HUMAN, MESSAGE, None), stop_checker)
    stop_checker_sub_2 = kp.load_subscribe_RDF(Triple(AGENT, MESSAGE, None), stop_checker)

    kp.load_rdf_insert(Triple(HUMAN, MESSAGE, URI(text)))

    speakers = [Speaker(i, kp) for i in range(0, SPEAKERS_NUM)]
    subs = [kp.load_subscribe_RDF(Triple(AGENT, MESSAGE, None), speaker) for speaker in speakers]

    kp.load_rdf_insert(Triple(AGENT, MESSAGE, Literal(0)))

    while not stop_checker.is_stopped():
        sleep(1)

    list(map(kp.load_unsubscribe, subs + [stop_checker_sub_1, stop_checker_sub_2]))

    kp.clean_sib()
    kp.leave()

    raise os._exit(0)


class StopChecker:
    def __init__(self, kp: m3_kp_api = None):
        self.kp = kp
        self.words_length = None
        self.stopped = False

    def handle(self, added, removed):
        # print(f"---DEBUG--- StopChecker: {added=}")
        s, p, o = added[0]

        if s == HUMAN:
            self.words_length = len(o.value.split(' '))

        if s == AGENT:
            current_word_index = int(o.value)
            if current_word_index >= self.words_length:
                self.stopped = True

    def is_stopped(self):
        return self.stopped


class Speaker:
    def __init__(self, num: int, kp: m3_kp_api = None):
        self.num = num
        self.kp = kp

    def handle(self, added, removed):
        # print(f"---DEBUG--- Agent{self.num}: {added=}")

        s, p, o = added[0]
        current_word_index = int(o.value)
        if current_word_index % SPEAKERS_NUM == self.num:
            # print(f"---DEBUG--- Agent{self.num}: it's mine {added=}")

            self.kp.load_query_rdf(Triple(HUMAN, MESSAGE, None))
            s, p, o = self.kp.result_rdf_query[0]
            words = o.value.split(' ')
            if current_word_index < len(words):
                print(f"Agent_{self.num} {datetime.now()}: {words[current_word_index]}")
                self.kp.load_rdf_insert(Triple(AGENT, MESSAGE, Literal(current_word_index + 1)))


if __name__ == '__main__':
    main()
