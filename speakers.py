from smart_m3.m3_kp_api import *
from time import sleep
from datetime import datetime
import os

AGENT = URI("agent")
HUMAN = URI("human")
SYSTEM = URI("system")
MESSAGE = URI("message")
BEGIN = URI("BEGIN")
END = URI("END")
SPEAKERS_NUM = 3


def main():
    kp = m3_kp_api()
    kp.clean_sib()

    text = input("Input your text here: ")

    kp.load_rdf_insert(Triple(HUMAN, MESSAGE, URI(text)))
    kp.load_rdf_insert(Triple(SYSTEM, MESSAGE, BEGIN))

    speakers = [Speaker(i, kp) for i in range(0, SPEAKERS_NUM)]
    subs = [kp.load_subscribe_RDF(Triple(AGENT, MESSAGE, None), speaker) for speaker in speakers]

    kp.load_rdf_insert(Triple(AGENT, MESSAGE, Literal(0)))

    while True:
        kp.load_query_rdf(Triple(SYSTEM, MESSAGE, None))
        s, p, o = kp.result_rdf_query[0]
        if o == END:
            break
        sleep(1)

    list(map(kp.load_unsubscribe, subs))

    kp.clean_sib()
    kp.leave()

    raise os._exit(0)


class Speaker:
    def __init__(self, num: int, kp: m3_kp_api = None):
        self.num = num
        self.kp = kp

    def handle(self, added, removed):
        # print(f"---DEBUG--- Agent{self.num}: {added=}")

        s, p, o = added[0]
        current_word_index = int(o.value)
        if current_word_index % SPEAKERS_NUM == self.num:
            # print(f"---DEBUG--- Agent{self.num}: it's mine {added=}")

            self.kp.load_query_rdf(Triple(HUMAN, MESSAGE, None))
            s, p, o = self.kp.result_rdf_query[0]
            words = o.value.split(' ')
            if current_word_index < len(words):
                print(f"Agent_{self.num} {datetime.now()}: {words[current_word_index]}")
                self.kp.load_rdf_insert(Triple(AGENT, MESSAGE, Literal(current_word_index + 1)))
            else:
                self.kp.load_rdf_update([Triple(SYSTEM, MESSAGE, END)], [Triple(SYSTEM, MESSAGE, BEGIN)])


if __name__ == '__main__':
    main()
